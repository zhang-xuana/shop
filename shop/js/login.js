
$('#login').on('click' , function () {
    const name = $('#user-login').val()
    const pwd = $('#pass-login').val()
    let userList = localStorage.getItem('userList')
    if (userList) {
        userList = JSON.parse(userList)
        const isExist = userList.some(user => {
            return user.name === name && user.pwd === pwd
        })
        if (isExist) {
            // 存cookie（存根目录）
            // TODO: 7天免登录
            $.cookie('name',name, { expires: 3, path: '/' })
            
            alert('登录成功，即将跳转首页')
            window.open('../index.html', '_self')
            
            
        } else {
            alert('用户名或密码错误，请重试')
        }
    } else {
        // 数据里没有注册过用户
        alert('一个用户都没有，请先注册')
        location.replace('./register.html')
    }

})