// 把用户信息存入localStorage
// const  $name = $('#user-register').val()
// const $pwd = $('#pass-register').val()
$('#register').on ('click',function () {
    const userinfo = {
      name: $('#user-register').val(),
      pwd: $('#pass-register').val()
    }
    console.log(userinfo)
    // 先取
    // 判断是否已经有数据了
    let userList = localStorage.getItem('userList')
    if (userList) {
      // 已经注册过其他用户了
      // 取出来的字符串解析成数组
      userList = JSON.parse(userList)
      // 把现在这条用户信息push到list里
      userList.push(userinfo)
      localStorage.setItem('userList', JSON.stringify(userList))
    } else {
      // 一个用户都还没有注册过
      // 存入只有userinfo这一条数据的数组
      localStorage.setItem('userList', JSON.stringify([userinfo]))
    }
    alert('注册成功，即将跳转登录页')
    location.href = 'http://localhost/work/shop/html/login.html'
  })
