// 点击添加按钮，把数据发送给后端

const $btn = $('#btn-add')
// 事件绑定，在项目里不怎么好，因为绑定只能绑定一个，项目里一般都有很多人
// 如果都绑定就会覆盖，以最后绑定的那个人为准
// btn.onclick = function(){

// }

// 所以用事件监听
$btn.on('click', ()=> {
    const name = $('#inputName').val(),
        price = $('#inputPrice').val(),
        num = $('#inputNum').val()

    // 发送请求
    $.ajax({
        url:'./api/shop/add.php',
        data: {
            name,
            price,
            num
        },
        success:(resp)=>{
            $('#msgWrap').html(resp.body.msg) 
            getData()
            setTimeout(() => {
                $('#msgWrap').html('')
                // 隐藏模态框
                $('#addModal').modal('hide')
                
            }, 2000)
        },
        error:(resp)=>{
            $('#msgWrap').html(resp.body.msg)
        },
        dataType:'json'
    })
})
$('#addModal').on('hidden.bs.modal', function (e) {
    $('#inputName').val('')
    $('#inputPrice').val('') 
    $('#inputNum').val('')
})