const utils = {
    /**
     * 存 cookie
     * @param {string} key        要存的 cookie 名称
     * @param {string} value       要存的cookie 的值
     * @param {object} option       {expires ：7，path：'/'} 代表存一个 7 天过期的根目录的 cookie
     */
    setCookie: function (key, value, option) {
        var str = `${key} = ${encodeURIComponent(value)}`
        // 如果有 option
        if (option) {
            // 如果有 path 目录
            if (option.path) {
                str += `;path=${option.path}`
            }
            // 如果有过期时间
            if (option.expires) {
                // 获取当前时间
                var date = new Date()
                // 设置时间为 当前时间 加上 过期时间
                date.setDate(date.getDate() + option.expires)
                // 时间转化为标准时区
                str += `;expires = ${date.toUTCString()}`
            }
        }
        document.cookie = str
    },
    /**
     * 取cookie
     * @param {string} [key] 要取的cookie的名称，如果不传返回所有cookie
     * @return {string || object}  取出来的cookie的值，如果没有 key 返回整个对象
     */
    getCookie(key) {
        var str = document.cookie
        var cookies = str.split('; ')
        // 遍历cookies数组，每一个元素再按照=拆开成属性名和属性值
        var obj = {}
        cookies.forEach(cookie => {
            var arr = cookie.split('=')
            // arr[0]作为属性名，arr[1]作为属性值放入obj
            // 取的时候把属性值解码
            obj[arr[0]] = decodeURIComponent(arr[1])
        })
        return key ? obj[key] : obj
    },

    /**
     * 删除cookie
     * @param {string} key 要删除的cooie的名称
     * @param {string} [path] 可选参数，cookie的路径 
     */
    removeCookie(key, path) {
        var date = new Date()
        date.setDate(date.getDate() - 1)
        var str = `${key}=0;expires=${date.toUTCString()}`
        if (path) {
            str += `;path=${path}`
        }
        document.cookie = str
    },
    /**
     * 
     * @param {string} url         请求的地址
     * @param {object} query        请求携带的参数
     * @param {function} fn         成功之后的回调函数
     * @param {boolean} isJson      返回的数据是否为JSON 格式，默认为true
     */
    get(url, query, fn, isJson = true) {
        if (query) {
            url += '?'
            for (var key in query) {
                url += `${key}=${query[key]}&`
            }
            // 去掉最后一个&
            url = url.slice(0, -1)
        }
        var xhr = new XMLHttpRequest()
        xhr.open('get', url)
        xhr.send()
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    var data = isJson ? JSON.parse(xhr.responseText) : xhr.responseText
                    fn && fn(data)
                }
            }
        }
    },
    /**
     * 
     * @param {string} url         请求的地址
     * @param {object} query        请求携带的参数
     * @param {function} fn         成功之后的回调函数
     * @param {boolean} isJson      返回的数据是否为JSON 格式，默认为true
     */
    post(url, query, fn, isJson = true) {
        var str = ''
        if (query) {
            for (var key in query) {
                str += `${key}=${query[key]}&`
            }
            // 去掉最后一个&
            str = str.slice(0, -1)
        }
        var xhr = new XMLHttpRequest()
        xhr.open('post', url)
        // 设置请求头，在open之后send之前
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
        xhr.send(str)
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    var data = isJson ? JSON.parse(xhr.responseText) : xhr.responseText
                    fn && fn(data)
                }
            }
        }
    },
    /**
     * @param {string} url
     * @param {string} method 
     * @param {object} query   
     * @param {function} fn      
     * @param {boolean} isJson
     */

    // ajax(url,method, query, fn,isJson = true) {
    //     var str = ''
    //     if (query) {
    //         if (method === 'get') {
    //             str += '?'
    //         }
    //         for (var key in query) {
    //             str += `${key}=${query[key]}&`
    //         }
    //         str = str.slice(0, -1)
    //         if (method === 'get') {
    //             url += str
    //             str = ''
    //         }
    //     }
    //     var xhr = new XMLHttpRequest()
    //     xhr.open(method, url)
    //     if (method === 'post') {
    //         xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
    //     }
    //     xhr.send(str)
    //     xhr.onreadystatechange = function () {
    //         if (xhr.readyState === 4) {
    //             if (xhr.status === 200) {
    //                 var data = isJson ? JSON.parse(xhr.responseText) : xhr.responseText
    //                 fn && fn(data)
    //             }
    //         }
    //     }
    // }
    ajax(option, query, fn,isJson = true) {
        var str = ''
        if (query) {
            if (option.method === 'get') {
                str += '?'
            }
            for (var key in query) {
                str += `${key}=${query[key]}&`
            }
            str = str.slice(0, -1)
            if (option.method === 'get') {
                option.url += str
                str = ''
            }
        }
        var xhr = new XMLHttpRequest()
        xhr.open(option.method, option.url)
        if (option.method === 'post') {
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
        }
        xhr.send(str)
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    var data = isJson ? JSON.parse(xhr.responseText) : xhr.responseText
                    fn && fn(data)
                }
            }
        }
    },
    /**
     * 
     * @param {string} url  其他接口地址
     * @param {string} cbName   全局回调函数名
     * @param {object} query   其他的一些参数
     */
    jsonp(url,cbName,query){
        // 创建script
        url += `?cb=${cbName}`
        if(query){
            for(var key in query){
                //空格这些没有就别写上去
                url += `&${key}=${query[key]}`
            }
        }
        var script = document.createElement('script')
        script.src = url
        document.body.appendChild(script)
        // 由于script请求异步，所以只要存在就行，请求发出去之后就可以移除了
        document.body.removeChild(script)
    },
    /**
     * 基于promise 的 Ajax 的 get 请求
     * @param {string} url      请求的地址
     * @param {object} query    请求携带的参数
     * @param {boolean} isJson  返回数据是否为一个json 格式，默认为true
     */
    promise_get(url ,query ,isJson = true){
        if(query){
            url += '?'
            for(var key in query){
                url += `${key}=${query[key]}&`
            }
            url = url.slice(0,-1)
        }
        // 在这里把promise的实例return出去，这样将来调用这个方法的时候，后面就可以直接.then
        return new Promise((resolve,reject)=>{
            var xhr = new XMLHttpRequest()
            xhr.open('get',url)
            xhr.send()
            xhr.onreadystatechange = function(){
                if(xhr.readyState === 4){
                    if(xhr.status ===200 ){
                        var data  = isJson?JSON.parse(xhr.responseText):xhr.responseText
                        resolve(data)
                    }else{
                        reject()
                    }
                }
            }
        })
    }
}