// 使用事件委托给按钮添加事件

$('#tbody-shop').on('click', 'button', function () {
    const $tr = $(this).parents('tr')
    if ($(this).hasClass('btn-edit')) {
        // 编辑按钮
        $tr.addClass('edit').find('span').each(function () {
            $(this).next().val($(this).html())
        })
    } else if ($(this).is('.btn-ok')) {
        const Id = $tr.data('id')
        const name = $tr.find('.input-name').val()
        const price = $tr.find('.input-price').val()
        const num = $tr.find('.input-num').val()
        $.ajax({
            url: './api/shop/update.php',
            data: {
                Id,
                name,
                price,
                num
            },
            success: (resp) => {
                $tr.removeClass('edit').find('span').each(function(){
                    $(this).html($(this).next().val())
                })
            },
            dataType: 'json'
        })
    } else if ($(this).hasClass('btn-cancel')) {
        $tr.removeClass('edit')
    } else if ($(this).hasClass('btn-del')) {
        // 删除
        const Id = $tr.data('id')
        $.ajax({
            url: './api/shop/delete.php',
            data: {
                Id
            },
            success: (resp) => {
                getData()
            },
            dataType: 'json'
        })
    }
})




// tbody.on('click',function (e) {
//     const target = e.target
//     const tr = target.parentNode.parentNode
//     // includes时ES6新增的数组API，是否包含某个元素返回布尔值
//     if (Array.from(target.classList).includes('btn-edit')) {
//         // 编辑
//         // 给tr加上edit class，把每一个span的innerHTML给对应input的value

//         tr.classList.add('edit')
//         const spans = tr.querySelectorAll('span')
//         spans.forEach(span => {
//             span.nextElementSibling.value = span.innerHTML
//         })
//     } else if (Array.from(target.classList).includes('btn-ok')) {
//         // 确定按钮
//         const Id = tr.getAttribute('data-id')
//         const name = tr.querySelector('.input-name').value
//         const price = tr.querySelector('.input-price').value
//         const num = tr.querySelector('.input-num').value

// $.ajax({
//     url:'./api/shop/update.php',
//     data:{
//         Id,
//     name,
//     price,
//     num
//     },
//     success:(resp)=>{
//         tr.classList.remove('edit')
//         const spans = tr.querySelectorAll('span')
//         spans.forEach(span => {
//             span.innerHTML = span.nextElementSibling.value
//         })
//     },
//     dataType:'json'
// })
//     }else if (Array.from(target.classList).includes('btn-cancel')) {
//         tr.classList.remove('edit')
//     }
//     else if (Array.from(target.classList).includes('btn-del')) {
//         const Id = tr.getAttribute('data-id')
//         $.ajax({
//             url:'./api/shop/delete.php',
//             data:{
//                 Id
//             },
//             success:(resp)=>{
//                 getData()
//             },
//             dataType:'json'
//         })
//     }
// })