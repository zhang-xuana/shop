// 点击添加按钮，把数据发送给后端

const btn = document.querySelector('#btn-add'),
  inputName = document.querySelector('#inputName'),
  inputPrice = document.querySelector('#inputPrice'),
  inputNum = document.querySelector('#inputNum'),
  msgWrap = document.querySelector('#msgWrap')

// 事件绑定，在项目里不怎么好，因为绑定只能绑定一个，项目里一般都有很多人
// 如果都绑定就会覆盖，以最后绑定的那个人为准
// btn.onclick = function(){

// }

// 所以用事件监听
btn.addEventListener('click', function () {
    const name = inputName.value,
        price = inputPrice.value,
        num = inputNum.value

    // 发送请求
    utils.promise_get('./api/shop/add.php', {
        name,
        price,
        num
    }).then(resp => {
        if (resp.code === 200) {
            msgWrap.innerHTML = resp.body.msg
            // classList 获取class列表，add可以原基础之上添加class，remove可以在原基础之上移除class
            msgWrap.classList.add('text-success')
            getData()
            setTimeout(() => {
                msgWrap.innerHTML = ''
                msgWrap.classList.remove('text-success')
                // 隐藏模态框
                $('#addModal').modal('hide')
                
            }, 2000)
            
        } 
        else {
            // 添加失败
            msgWrap.innerHTML = resp.body.msg
            msgWrap.classList.add('text-danger')
            setTimeout(() => {
                msgWrap.innerHTML = ''
                msgWrap.classList.remove('text-danger')
            }, 2000)
        }
    })
})
$('#addModal').on('hidden.bs.modal', function (e) {
    inputName.value = inputPrice.value = inputNum.value = ''
})