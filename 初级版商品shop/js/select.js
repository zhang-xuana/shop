// 发送请求获取商品列表渲染 table
const tbody = document.querySelector('#tbody-shop')
getData()

function getData() {

    // 请求的路径不需要返回上一级，因为请求是从首页出发的
    utils.promise_get('api/shop/select.php').then(resp => {
        if (resp.code === 200) {
            const {
                list
            } = resp.body
            let str = ``
            list.forEach((shop, index) => {
                str +=
                `
                    <tr data-id="${shop.Id}">
                        <td>${index+1}</td>
                        <td><span>${shop.name}</span><input type="text" class="input-name"></td>
                        <td><span>${shop.price}</span><input type="text" class="input-price"></td>
                        <td><span>${shop.num}</span><input type="text" class="input-num"></td>
                        <td>
                            <button class="btn btn-xs btn-info btn-edit">编辑</button>
                            <button class="btn btn-xs btn-danger btn-del">删除</button>
                            <button class="btn btn-xs btn-success btn-ok">确定</button>
                            <button class="btn btn-xs btn-warning btn-cancel">取消</button>
                        </td>
                    </tr>
                `
            })
            tbody.innerHTML = str
        }
    })
}