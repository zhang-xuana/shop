// 使用事件委托给按钮添加事件
tbody.addEventListener('click', function (e) {
    const target = e.target
    const tr = target.parentNode.parentNode
    // includes时ES6新增的数组API，是否包含某个元素返回布尔值
    if (Array.from(target.classList).includes('btn-edit')) {
        // 编辑
        // 给tr加上edit class，把每一个span的innerHTML给对应input的value

        tr.classList.add('edit')
        const spans = tr.querySelectorAll('span')
        spans.forEach(span => {
            span.nextElementSibling.value = span.innerHTML
        })
    } else if (Array.from(target.classList).includes('btn-ok')) {
        // 确定按钮
        const Id = tr.getAttribute('data-id')
        const name = tr.querySelector('.input-name').value
        const price = tr.querySelector('.input-price').value
        const num = tr.querySelector('.input-num').value

        utils.promise_get('./api/shop/update.php', {
            Id,
            name,
            price,
            num
        }).then(resp => {
            if (resp.code === 200) {
                // 把tr恢复初始状态，把input的value值赋给对应的span
                tr.classList.remove('edit')
                const spans = tr.querySelectorAll('span')
                spans.forEach(span => {
                    span.innerHTML = span.nextElementSibling.value
                })
            }
        })
    }else if (Array.from(target.classList).includes('btn-cancel')) {
        tr.classList.remove('edit')
    }
    else if (Array.from(target.classList).includes('btn-del')) {
        const Id = tr.getAttribute('data-id')
        utils.promise_get('./api/shop/delete.php', {
            Id
        }).then(resp => {
            if (resp.code === 200) {
                getData()
            }
        })
    }
})